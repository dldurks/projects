CSE-20189-SP16 - Projects
=========================

This is the group repository for the final two projects in [CSE-20189-SP16].

Group Members
-------------

Project 01:
- David Durkin   (ddurkin2@nd.edu)
- Matt Reilly    (mreill11@nd.edu)

Project 02:
- David Durkin (ddurkin2@nd.edu)
- Matt Reilly (mreill11@nd.edu)
- Chris Beaufils (cbeaufil@nd.edu)

[CSE-20189-SP16]: https://www3.nd.edu/~pbui/teaching/cse.20189.sp16/
